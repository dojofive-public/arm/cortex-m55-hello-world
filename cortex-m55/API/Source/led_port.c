/*
 * Copyright (c) 2019 Arm Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "led_port.h"
#include "device_definition.h"
#include "cmsis.h"

#define LED_PORT_WIDTH          8U
#define LED_PORT_MASK           ((1U << LED_PORT_WIDTH) - 1)
#define LED_PORT_NO_LED_SET     32U

#define LED_PORT_SET_SUCCESS    0U
#define LED_PORT_SET_FAILED     1U


static uint32_t find_first_bit(uint32_t value)
{
    return __CLZ(__RBIT(value));
}

unsigned int get_led_port_bit_length(void)
{
    return LED_PORT_WIDTH;
}

void led_port_init(void)
{
    /* Not needed for this target */
}

unsigned int set_led_port(unsigned int led_mask)
{
    uint32_t led = 0;
    if (led_mask <= LED_PORT_MASK) {
        /* Clear all LEDs first */
        arm_mps2_io_write_leds(&ARM_MPS2_IO_SCC_DEV,
                               ARM_MPS2_IO_ACCESS_PORT, 0U, 0U);
        /* Led value is a bitmask, find the first bit which is set */
        led = find_first_bit(led_mask);
        while (led != LED_PORT_NO_LED_SET) {
            arm_mps2_io_write_leds(&ARM_MPS2_IO_SCC_DEV,
                                   ARM_MPS2_IO_ACCESS_PIN, led, 1U);
            led_mask &= ~(1u << led);
            led = find_first_bit(led_mask);
        }
        return LED_PORT_SET_SUCCESS;
    }
    return LED_PORT_SET_FAILED;
}

unsigned int get_led_port(void)
{
    return (arm_mps2_io_read_leds(&ARM_MPS2_IO_SCC_DEV,
                                  ARM_MPS2_IO_ACCESS_PORT, 0));
}
