#!/bin/bash

# Path on the docker container where the project repo is cloned to, 
# unlikely to change
git_clone_path="/builds/arm/"

# Name of the directory where the project repo is cloned to, 
# comes from the project name on EmbedOps
project_repo="cortex-m55-hello-world"

# Path to the application file that runs with the Corstone-300 model, 
# will change if different source file is used
application_file="cortex-m55/Objects/Blinky.axf"

# Path to the configuration file that will be used by the model
config_file="cortex-m55/config-ci.txt"

# Path at which to place the UART output artifact
uart_output_file="cortex-m55/uartOutputFile"

# Build up the full paths to the files to be used later
application_path="${git_clone_path}${project_repo}/${application_file}"
config_path="${git_clone_path}${project_repo}/${config_file}"
uart_output_file_path="${git_clone_path}${project_repo}/${uart_output_file}"

# Start the model with the AXF object file and the config file
# Limit the simulation to one minute and place in background to 
# execute telnet commands
cd /usr/local/FVP_Corstone_SSE-300/models/Linux64_GCC-6.4
./FVP_Corstone_SSE-300 -a "${application_path}" -f "${config_path}" --simlimit 60 &

# Allow for the model to run
sleep 3

# Open a telnet session, connect to the model, and look for the telnet output
function fn() {
  EXPECTED_OUTPUT="Blinky is running"
  
  echo "The code is running in the model..."
  
  unbuffer nc localhost 5003 -w55
  
  grep -i "${EXPECTED_OUTPUT}" "${uart_output_file_path}"
  status=$?
  
  if [ $status -eq 0 ]; then
    echo "Expected output received"
  else
    echo "Expected output: '${EXPECTED_OUTPUT}' but received "
    cat "${uart_output_file_path}"
  fi
  
  return $status
  }

fn
