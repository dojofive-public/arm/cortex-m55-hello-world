#!/bin/bash

# This is a fake script that could be replaced with
# a real one which would check out the necessary ARM
# license required to compile a project for the Corstone
# SSE-300 FVP model when it is not detected on the system
if [[ -z "${ARMLMD_LICENSE_FILE}" ]]
then
  echo "No license file set, move on to simulation"
else
  echo "Checking out license to compile source"
fi
