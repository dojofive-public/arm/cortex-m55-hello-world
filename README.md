# Cortex-M55 Hello World
This project is meant demonstrate testing a "Blinky" example project against the Corstone SSE-300 FVP model in a CI/CD pipeline. 
The model is installed on the docker image that can be used in a CI/CD pipeline such as this, or locally on your machine. 
We also provided the pre-compiled source code for the simulator. That way, if an Arm license server isn't detected on your system for a compiler, so you can still test the pipeline.

## Usage
To use or change this project, simply clone it on a local machine, edit `Blinky.c` (or add your own source files) and then rebuild the project. That can be done with three tools:
 * Arm Development Studio (v2020.0)
 * Keil uVision IDE (Part of Keil MDK and MDK Lite v 5.31)
 * Arm Compiler 6 standalone

The source code for this example project comes from [this project on Keil's site](https://www.keil.com/boards2/arm/v2m_mps2_sse_300_fvp/) and comes as part of the Arm V2M-MPS2 Board Support PACK which can be installed from Keil's built-in Pack Manager utility.

When CI/CD runs on this project, the `build` stage will find the appropriate file to pass to the simulation stage, which will run the `.axf` file against the Corstone SSE-300 model and give test output.

If you have built custom models, or have a different processor FVP, you can follow the instructions to create a similar testing pipeline on an upcoming blog post. This README will be updated with the link once it is published.