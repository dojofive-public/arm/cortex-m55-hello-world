FROM buildpack-deps:18.04

LABEL "org.opencontainers.image.title"="ARM Corstone-300 FVP"
LABEL "org.opencontainers.image.description"="All of the tools needed to run compiled projects on a Cortex M-55 model."
LABEL "org.opencontainers.image.vendor"="Dojo Five, LLC"
LABEL "org.opencontainers.image.authors"="Anders Culver"
# Labels for documentation, source, revision, and created are set dynamically in the .embedops-ci.yml file

# Set the time so the simulator doesn't exit due to time mismatch errors.
ENV TZ=America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Update package database
RUN apt-get update -y

# Get libdbus and xvfb, needed for model execution
RUN apt-get install --no-install-recommends --no-upgrade libdbus-1-3 xvfb netcat expect tcl -y

# Use curl to grab Corstone-300 model, unzip move to /usr/local/ and remove the archive to reduce image size
RUN curl -L "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-300/FVP_Corstone_SSE-300_11.10_44.tgz?revision=c1fd21ac-71f1-408f-88cc-a4c56d561b6b&la=en&hash=27E720C78BAD103BE3B05A26FE4363E3137FC31B" -o FVP_Corstone_SSE_300.tgz && \
        tar xvf FVP_Corstone_SSE_300.tgz -C /usr/local/ && \
        rm FVP_Corstone_SSE_300.tgz

# Install model, with a few options to bypass installer prompts
RUN sh /usr/local/FVP_Corstone_SSE-300.sh --i-agree-to-the-contained-eula --no-interactive

# Keep this directory for sharing files with host
RUN mkdir -p /host/files
